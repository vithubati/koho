.PHONY: build
build:
	go build -o bin/koho

.PHONY: run
run:
	./bin/koho

.PHONY: test
test:
	cd pkg/transaction; go test
