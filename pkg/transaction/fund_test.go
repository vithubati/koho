package transaction

import (
	"bitbucket.org/vithubati/koho/pkg/db"
	"reflect"
	"testing"
)

func TestHandler_IsDuplicate(t *testing.T) {
	testData := Fund{
		ID:         "15887",
		CustomerID: "528",
		Amount:     "$3318.47",
		Time:       "2000-01-01T00:00:00Z",
	}
	type fields struct {
		DB *db.DB
	}
	type args struct {
		fund Fund
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name:   "IsDuplicateTestForFalse",
			fields: fields{DB: db.InitDB()},
			args:   args{fund: testData},
			want:   false,
		},
		{
			name:   "IsDuplicateTestForTrue",
			fields: fields{DB: db.InitDB()},
			args:   args{fund: testData},
			want:   true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := &Handler{
				DB: tt.fields.DB,
			}
			if got := h.IsDuplicate(tt.args.fund); got != tt.want {
				t.Errorf("IsDuplicate() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHandler_exceedMaxLimit(t *testing.T) {
	testData := Fund{
		ID:         "15887",
		CustomerID: "528",
		LoadAmount: 3318.47,
		Time:       "2000-01-01T00:00:00Z",
	}
	testData2 := Fund{
		ID:         "15889",
		CustomerID: "528",
		LoadAmount: 5318.47,
		Time:       "2000-01-01T00:00:00Z",
	}
	type fields struct {
		DB *db.DB
	}
	type args struct {
		fund Fund
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name:   "exceedMaxLimitTestForFalse",
			fields: fields{DB: db.InitDB()},
			args:   args{fund: testData},
			want:   false,
		},
		{
			name:   "exceedMaxLimitTestForTrue",
			fields: fields{DB: db.InitDB()},
			args:   args{fund: testData2},
			want:   true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := &Handler{
				DB: tt.fields.DB,
			}
			if got := h.exceedMaxLimit(tt.args.fund); got != tt.want {
				t.Errorf("exceedMaxLimit() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHandler_canDoDayTransaction(t *testing.T) {
	dB := db.InitDB()
	testData := Fund{
		ID:         "15887",
		CustomerID: "528",
		LoadAmount: 3318.47,
		Time:       "2000-01-01T00:00:00Z",
		Date:       "2000-01-01",
		WeekOfYear: "2000-40",
	}
	testData2 := Fund{
		ID:         "15889",
		CustomerID: "528",
		LoadAmount: 3318.47,
		Time:       "2000-01-01T00:00:00Z",
		Date:       "2000-01-01",
		WeekOfYear: "2000-40",
	}
	testData3 := Fund{
		ID:         "15890",
		CustomerID: "528",
		LoadAmount: 200.47,
		Time:       "2000-01-01T00:00:00Z",
		Date:       "2000-01-01",
		WeekOfYear: "2000-40",
	}
	testData4 := Fund{
		ID:         "15891",
		CustomerID: "528",
		LoadAmount: 200.47,
		Time:       "2000-01-01T00:00:00Z",
		Date:       "2000-01-01",
		WeekOfYear: "2000-40",
	}
	testData5 := Fund{
		ID:         "15892",
		CustomerID: "528",
		LoadAmount: 200.47,
		Time:       "2000-01-01T00:00:00Z",
		Date:       "2000-01-01",
		WeekOfYear: "2000-40",
	}
	type fields struct {
		DB *db.DB
	}
	type args struct {
		fund Fund
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name:   "canDoDayTransactionTrue",
			fields: fields{DB: dB},
			args:   args{fund: testData},
			want:   true,
		},
		{
			name:   "canDoDayTransactionFalse",
			fields: fields{DB: dB},
			args:   args{fund: testData2},
			want:   false,
		},
		{
			name:   "canDoDayTransactionForMaxTransactionPerDay2",
			fields: fields{DB: dB},
			args:   args{fund: testData3},
			want:   true,
		},
		{
			name:   "canDoDayTransactionForMaxTransactionPerDay3",
			fields: fields{DB: dB},
			args:   args{fund: testData4},
			want:   true,
		},
		{
			name:   "canDoDayTransactionForMaxTransactionPerDay4",
			fields: fields{DB: dB},
			args:   args{fund: testData5},
			want:   false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := &Handler{
				DB: tt.fields.DB,
			}
			got := h.canDoDayTransaction(tt.args.fund)
			if got != tt.want {
				t.Errorf("canDoDayTransaction() = %v, want %v", got, tt.want)
				return
			}
			if got {
				h.handleDayTransaction(tt.args.fund)
			}
		})
	}
}

func TestHandler_canDoWeeklyTransaction(t *testing.T) {
	dB := db.InitDB()
	testData := Fund{
		ID:         "15887",
		CustomerID: "528",
		LoadAmount: 5000.00,
		Time:       "2000-01-01T00:00:00Z",
		Date:       "2000-01-01",
		WeekOfYear: "2000-40",
	}
	testData2 := Fund{
		ID:         "15889",
		CustomerID: "528",
		LoadAmount: 5000.00,
		Time:       "2000-01-01T00:00:00Z",
		Date:       "2000-01-01",
		WeekOfYear: "2000-40",
	}
	testData3 := Fund{
		ID:         "15890",
		CustomerID: "528",
		LoadAmount: 5000.00,
		Time:       "2000-01-01T00:00:00Z",
		Date:       "2000-01-01",
		WeekOfYear: "2000-40",
	}
	testData4 := Fund{
		ID:         "15891",
		CustomerID: "528",
		LoadAmount: 5000.00,
		Time:       "2000-01-01T00:00:00Z",
		Date:       "2000-01-01",
		WeekOfYear: "2000-40",
	}
	testData5 := Fund{
		ID:         "15892",
		CustomerID: "528",
		LoadAmount: 200.47,
		Time:       "2000-01-01T00:00:00Z",
		Date:       "2000-01-01",
		WeekOfYear: "2000-40",
	}
	type fields struct {
		DB *db.DB
	}
	type args struct {
		fund Fund
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name:   "canDoWeeklyTransactionTrue",
			fields: fields{DB: dB},
			args:   args{fund: testData},
			want:   true,
		},
		{
			name:   "canDoWeeklyTransactionTrue2",
			fields: fields{DB: dB},
			args:   args{fund: testData2},
			want:   true,
		},
		{
			name:   "canDoWeeklyTransactionTrue3",
			fields: fields{DB: dB},
			args:   args{fund: testData3},
			want:   true,
		},
		{
			name:   "canDoWeeklyTransactionTrue4",
			fields: fields{DB: dB},
			args:   args{fund: testData4},
			want:   true,
		},
		{
			name:   "canDoWeeklyTransactionFalse",
			fields: fields{DB: dB},
			args:   args{fund: testData5},
			want:   false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := &Handler{
				DB: tt.fields.DB,
			}
			got := h.canDoWeeklyTransaction(tt.args.fund)
			if got != tt.want {
				t.Errorf("canDoWeeklyTransaction() = %v, want %v", got, tt.want)
			}
			if got {
				h.handleDayTransaction(tt.args.fund)
				h.handleWeekTransaction(tt.args.fund)
			}
		})
	}
}

func TestHandler_LoadFound(t *testing.T) {
	dB := db.InitDB()
	testData := Fund{
		ID:         "15857",
		CustomerID: "538",
		LoadAmount: 5000.00,
		Time:       "2000-01-01T00:00:00Z",
		Date:       "2000-01-01",
		WeekOfYear: "2000-40",
	}
	testData2 := Fund{
		ID:         "15859",
		CustomerID: "538",
		LoadAmount: 5000.00,
		Time:       "2000-01-01T00:00:00Z",
		Date:       "2000-01-01",
		WeekOfYear: "2000-40",
	}
	type fields struct {
		DB *db.DB
	}
	type args struct {
		fund Fund
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   LoadFoundResponse
	}{
		{
			name:   "AcceptTrue",
			fields: fields{DB: dB},
			args:   args{fund: testData},
			want:   LoadFoundResponse{
				ID:         testData.ID,
				CustomerID: testData.CustomerID,
				Accepted:   true,
			},
		},
		{
			name:   "AcceptFalse",
			fields: fields{DB: dB},
			args:   args{fund: testData2},
			want:   LoadFoundResponse{
				ID:         testData2.ID,
				CustomerID: testData2.CustomerID,
				Accepted:   false,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := &Handler{
				DB: tt.fields.DB,
			}
			if got := h.LoadFund(tt.args.fund); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("LoadFound() = %v, want %v", got, tt.want)
			}
		})
	}
}
