package transaction

import "bitbucket.org/vithubati/koho/pkg/db"

const (
	MaxAmountAllowedPerDay  = 5000.00
	MaxAmountAllowedPerWeek = 20000.00
	MaxAllowedPerDay        = 3
)

type Fund struct {
	ID         string  `json:"id"`
	CustomerID string  `json:"customer_id"`
	Amount     string  `json:"load_amount"`
	Time       string  `json:"time"`
	LoadAmount float64 `json:"-"`
	Date       string  `json:"-"`
	WeekOfYear string  `json:"-"`
}

type LoadFoundResponse struct {
	ID         string `json:"id"`
	CustomerID string `json:"customer_id"`
	Accepted   bool   `json:"accepted"`
}


// Velocity interface for handling loading funds
type Transaction interface {
	// LoadFound handle Load fund request
	LoadFund(fund Fund) LoadFoundResponse

	// IsDuplicate check for duplicate transactionId
	IsDuplicate(fund Fund) bool

	// exceedMaxLimit validate daily limit
	exceedMaxLimit(fund Fund) bool

	// handleDayTransaction handle daily Transaction
	handleDayTransaction(fund Fund)

	// increaseCount handle the transaction increment
	handleWeekTransaction(fund Fund)
}

type Handler struct {
	DB *db.DB
}

//NewTransactionHandler new instance to handle the Transaction
func NewTransactionHandler(db *db.DB) Transaction {
	return &Handler{DB: db}
}

// exceedMaxLimit validate daily limit
func (h *Handler) exceedMaxLimit(fund Fund) bool {
	return fund.LoadAmount > MaxAmountAllowedPerDay
}

// canDoDayTransaction validate daily Transaction
func (h *Handler) canDoDayTransaction(fund Fund) bool {
	data := h.DB.DayTransaction[fund.CustomerID][fund.Date]
	if (data.TotalAmount+fund.LoadAmount) > MaxAmountAllowedPerDay || (data.Count+1) > MaxAllowedPerDay {
		return false
	}
	return true
}

// canDoWeeklyTransaction validate weekly Transaction
func (h *Handler) canDoWeeklyTransaction(fund Fund) bool {
	data := h.DB.WeekTransaction[fund.CustomerID][fund.WeekOfYear]
	return (data.TotalAmount + fund.LoadAmount) <= MaxAmountAllowedPerWeek
}

// IsDuplicate check for duplicate transactionId
func (h *Handler) IsDuplicate(fund Fund) bool {
	if _, ok := h.DB.TransactionCount[fund.CustomerID]; ok {
		if _, ok := h.DB.TransactionCount[fund.CustomerID][fund.ID]; ok {
			return true
		} else {
			h.DB.TransactionCount[fund.CustomerID][fund.ID] = 1
		}
	} else {
		h.DB.TransactionCount[fund.CustomerID] = make(map[string]int)
		h.DB.TransactionCount[fund.CustomerID][fund.ID] = 1
	}
	return false
}

// handleDayTransaction handle daily Transaction
func (h *Handler) handleDayTransaction(fund Fund) {
	if _, ok := h.DB.DayTransaction[fund.CustomerID]; ok {
		// customer exist
		if today, ok := h.DB.DayTransaction[fund.CustomerID][fund.Date]; ok {
			// customer had already loaded for this day
			today.TotalAmount += fund.LoadAmount
			today.Count++
			// updating the today record
			h.DB.DayTransaction[fund.CustomerID][fund.Date] = today
		} else {
			// this is the first time customer is loading for today
			today = db.DayTransaction{
				ID:          fund.ID,
				Date:        fund.Date,
				Count:       1,
				TotalAmount: fund.LoadAmount,
			}
			h.DB.DayTransaction[fund.CustomerID][fund.Date] = today
		}
	} else {
		// customer doesn't exist
		h.DB.DayTransaction[fund.CustomerID] = make(map[string]db.DayTransaction)
		today := db.DayTransaction{
			ID:          fund.ID,
			Date:        fund.Date,
			Count:       1,
			TotalAmount: fund.LoadAmount,
		}
		h.DB.DayTransaction[fund.CustomerID][fund.Date] = today
	}
	h.increaseCount(fund)
}

// increaseCount handle the transaction increment
func (h Handler) increaseCount(fund Fund) {
	if _, ok := h.DB.TransactionCount[fund.CustomerID]; !ok {
		h.DB.TransactionCount[fund.CustomerID] = make(map[string]int)
		h.DB.TransactionCount[fund.CustomerID][fund.ID] = 1
	}
	h.DB.TransactionCount[fund.CustomerID][fund.ID]++
}

// handleWeekTransaction handle weekly Transaction
func (h *Handler) handleWeekTransaction(fund Fund) {
	if _, ok := h.DB.WeekTransaction[fund.CustomerID]; ok {
		// customer exist
		if today, ok := h.DB.WeekTransaction[fund.CustomerID][fund.WeekOfYear]; ok {
			today.TotalAmount += fund.LoadAmount
			// updating the today record
			h.DB.WeekTransaction[fund.CustomerID][fund.WeekOfYear] = today
		} else {
			// this is the first time customer is loading for today
			today = db.WeekTransaction{
				ID:          fund.ID,
				WeekOfYear:  fund.WeekOfYear,
				TotalAmount: fund.LoadAmount,
			}
			h.DB.WeekTransaction[fund.CustomerID][fund.WeekOfYear] = today
		}
	} else {
		// customer doesn't have weekly record yet
		h.DB.WeekTransaction[fund.CustomerID] = make(map[string]db.WeekTransaction)
		today := db.WeekTransaction{
			ID:          fund.ID,
			WeekOfYear:  fund.WeekOfYear,
			TotalAmount: fund.LoadAmount,
		}
		h.DB.WeekTransaction[fund.CustomerID][fund.WeekOfYear] = today
	}
}

// LoadFound handle Load fund request
func (h *Handler) LoadFund(fund Fund) LoadFoundResponse {
	response := LoadFoundResponse{
		ID:         fund.ID,
		CustomerID: fund.CustomerID,
		Accepted:   false,
	}
	if h.exceedMaxLimit(fund) || !h.canDoDayTransaction(fund) || !h.canDoWeeklyTransaction(fund) {
		return response
	}
	h.handleDayTransaction(fund)
	h.handleWeekTransaction(fund)
	response.Accepted = true
	return response
}
