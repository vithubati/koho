package db

import (
	"sync"
)

var once sync.Once
var Con *DB

type DayTransaction struct {
	ID   string
	Date string

	// total number of transactions
	Count int

	// total transaction amount
	TotalAmount float64
}

type WeekTransaction struct {
	ID string
	// total transaction amount
	TotalAmount float64
	WeekOfYear  string
}

type DB struct {
	DayTransaction   map[string]map[string]DayTransaction
	WeekTransaction  map[string]map[string]WeekTransaction
	TransactionCount map[string]map[string]int
}

func InitDB() *DB {
	once.Do(func() {
		Con = &DB{
			DayTransaction:   make(map[string]map[string]DayTransaction, 0),
			WeekTransaction:  make(map[string]map[string]WeekTransaction, 0),
			TransactionCount: make(map[string]map[string]int),
		}
	})
	return Con
}

