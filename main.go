package main

import (
	"bitbucket.org/vithubati/koho/pkg/db"
	"bitbucket.org/vithubati/koho/pkg/transaction"
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
	"time"
)

func main() {

	fptr := flag.String("fpath", "input.txt", "file path to read the input from")
	flag.Parse()

	file, err := os.Open(*fptr)
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		if err := file.Close(); err != nil {
			log.Fatal(err)
		}
	}()
	output, err := os.Create("./app_output.txt")
	if err != nil {
		log.Fatal(err)
	}
	writer := bufio.NewWriter(output)
	defer func() {
		if err := writer.Flush(); err != nil {
			log.Fatal(err)
		}
	}()
	scanner := bufio.NewScanner(file)

	handler := transaction.NewTransactionHandler(db.InitDB())
	for scanner.Scan() {
		fund := transaction.Fund{}
		if err := json.Unmarshal(scanner.Bytes(), &fund); err != nil {
			fmt.Println(fmt.Errorf("unmarshal error: %v", err.Error()).Error())
			log.Fatal(err)
		}

		amount, err := strconv.ParseFloat(fund.Amount[1:], 64)
		if err != nil {
			fmt.Println(fmt.Errorf("amount parse error: %v", err.Error()).Error())
			log.Fatal(err)
		}
		fund.LoadAmount = amount
		pTime, err := time.Parse(time.RFC3339, fund.Time)
		if err != nil {
			fmt.Println(fmt.Errorf("amount parse error: %v", err.Error()).Error())
			log.Fatal(err)
		}
		fund.Date = pTime.String()[:10]
		if handler.IsDuplicate(fund) {
			continue
		}
		year, week := pTime.ISOWeek()
		fund.WeekOfYear = fmt.Sprintf("%v-%v", year, week)
		res := handler.LoadFund(fund)
		ss, err := json.Marshal(res)
		if err != nil {
			fmt.Println(fmt.Errorf("MarshalIndent error: %v", err.Error()).Error())
		}
		//fmt.Println(string(ss))
		_, err = writer.WriteString(string(ss) + "\n")
		if err != nil {
			log.Fatal(err)
			return
		}
	}
	err = scanner.Err()
	if err != nil {
		log.Fatal(err)
	}
}
